"""core URL Configuration """

from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import RedirectView

from api.urls import router

urlpatterns = [

    path('admin/', admin.site.urls),

    path('', RedirectView.as_view(url='api/v1/')),

    path('api/auth/', include('rest_framework.urls')),

    path('api/v1/', include(router.urls)),

]
