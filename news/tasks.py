from datetime import datetime

from core.celery import app
from celery.utils.log import get_task_logger
from dateutil import parser
from django.utils import timezone
from feedparser import parse

from news.models import Feed, Article

logger = get_task_logger(__name__)

@app.task(bind=True)
def create_article(self, feed_id, title, desc, link, pub_date):
	logger.info('Creating article [%s]' %title)

	try:
		f = Feed.objects.get(id=feed_id)
		a = Article(feed=f, title=title, description=desc, link=link, published=pub_date)
		a.save()

		logger.info('Success: Article Created')
	except Exception as exc:
		logger.error('Could not create article: %s' %exc)

@app.task()
def sync_feed():
	logger.info('Syncing news feeds...')

	try:
		for f in Feed.objects.all():
			data = parse(f.link)
			for entry in data.entries:
				try:
					pub_date = parser.parse(entry.published)
				except Exception as e:
					pub_date = None

				if pub_date and pub_date > f.last_synced:
					create_article(f.id, entry.title, entry.description, entry.link, pub_date)

			f.last_synced = timezone.now()
			f.save()

			logger.info('Synced %s articles from %s' %(len(data.entries), f.title))
		logger.info('Successfully synced')
	except Exception as exc:
		logger.error('Could not sync news feed: %s' %exc)
	
