from django.contrib import admin

from . models import Category, Feed, Article


class CategoryAdmin(admin.ModelAdmin):
	list_display = ['id', 'title', 'created']


class FeedAdmin(admin.ModelAdmin):
	list_display = ['id', 'title', 'created']
	list_filter = ['created', 'title']

class ArticleAdmin(admin.ModelAdmin):
	list_display = ['id', 'title', 'published']
	list_filter = ['published']


admin.site.register(Category, CategoryAdmin)
admin.site.register(Feed, FeedAdmin)
admin.site.register(Article, ArticleAdmin)
