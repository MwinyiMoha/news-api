from django.db import models


class Category(models.Model):
	title = models.CharField(max_length=50)

	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name_plural = 'Categories'

	def __str__(self):
		return self.title

	@property
	def article_count(self):
		vals = []
		for feed in self.feed_set.all():
			vals.append(feed.article_set.count())

		return sum(vals)


class Feed(models.Model):
	category = models.ManyToManyField(Category)

	title = models.CharField(max_length=255, blank=True)
	description = models.TextField(blank=True)
	link = models.URLField()
	icon = models.URLField(blank=True)

	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	last_synced = models.DateTimeField(blank=True)

	def __str__(self):
		return self.title


class Article(models.Model):
	feed = models.ForeignKey(Feed, on_delete=models.CASCADE)

	title = models.CharField(max_length=255)
	description = models.TextField()
	link = models.URLField()
	published = models.DateTimeField()

	def __str__(self):
		return '%s ... %s' %(self.title, self.feed.title)
		
