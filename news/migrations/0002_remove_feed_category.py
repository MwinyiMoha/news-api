# Generated by Django 2.2.2 on 2019-06-24 14:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='feed',
            name='category',
        ),
    ]
