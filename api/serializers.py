from rest_framework import serializers

from news.models import Feed, Category, Article


class CategorySerializer(serializers.ModelSerializer):
	article_count = serializers.ReadOnlyField()

	class Meta:
		model = Category
		fields = '__all__'


class FeedSerializer(serializers.ModelSerializer):

	class Meta:
		model = Feed
		fields = '__all__'


class ArticleSerializer(serializers.ModelSerializer):

	class Meta:
		model = Article
		fields = '__all__'
