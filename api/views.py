from random import choices

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

from news.models import *
from . serializers import *


class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
	queryset = Category.objects.all()
	serializer_class = CategorySerializer


class FeedViewSet(viewsets.ModelViewSet):
	queryset = Feed.objects.all()
	serializer_class = FeedSerializer

	@action(detail=False, name='Featured News Desks')
	def featured_newsdesks(self, request):
		ids = choices(Feed.objects.values_list('pk', flat=True), k=2)
		qs = Feed.objects.filter(pk__in=ids)
		serializer = self.get_serializer(qs, many=True)

		return Response(serializer.data)


class ArticleViewSet(viewsets.ModelViewSet):
	queryset = Article.objects.all()
	serializer_class = ArticleSerializer
