from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from . models import Profile

User = get_user_model()

class CustomUserAdmin(UserAdmin):
	pass


class ProfileAdmin(admin.ModelAdmin):
	pass


admin.site.register(User, CustomUserAdmin)
admin.site.register(Profile, ProfileAdmin)
